//
//  ViewController.swift
//  education
//
//  Created by rodion on 2019-08-09.
//  Copyright © 2019 rodion. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, UISearchBarDelegate{
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serchBar.autocapitalizationType = .none
        serchBar.showsBookmarkButton = true
        loadRequest("https://www.google.com")
    }
    func loadRequest(_ string: String){
        let url = URL(string: string)
        webView.loadRequest(URLRequest(url: url!))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let addressText = searchBar.text else {return}
        searchBar.text = "https://" + addressText
        loadRequest("https://" + addressText)
    }
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        print("HISTORY")
    }
    @IBAction func forwardAction(_ sender: Any) {
        webView.goForward()
    }
    @IBAction func backAction(_ sender: Any) {
        webView.goBack()
    }
    @IBAction func refreshAction(_ sender: Any) {
        webView.reload()
    }
}
